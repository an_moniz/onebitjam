﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthController
{
    HealthModel _model;
    public HealthModel Model => _model;
    private HealthView _view;
    private TrainMovementController _movementController;

    public event Action CartDetachEvent;

    public HealthController()
    {
        _model = new HealthModel();
    }

    public void Initialize(HealthView view, TrainMovementController movementController)
    {
        _view = view;
        _movementController = movementController;
    }

    public void AddCart(ECartType cartType)
    {
        _model.AddCaboose(cartType);
        _view.DisplayCart(cartType, _model.Health - 1);
        if (cartType == ECartType.Heavy)
            _movementController.ModifyDrag(.1f);
    }

    public void DetachCart()
    {
        _view.HideCart(_model.Health - 1);
        ECartType oldCaboose = _model.RemoveCart();
        if (oldCaboose == ECartType.Heavy)
            _movementController.ModifyDrag(-.1f);
        CartDetachEvent?.Invoke();
    }
}