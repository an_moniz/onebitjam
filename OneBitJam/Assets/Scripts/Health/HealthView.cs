﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthView : MonoBehaviour
{
    [SerializeField] List<TrainCarView> _trainCarts = new List<TrainCarView>();
    public List<TrainCarView> TrainCartViews => _trainCarts;

    HealthController _healthController;
    public HealthController HealthController => _healthController;

    // Start is called before the first frame update
    void Start()
    {
        _healthController = new HealthController();
        _healthController.Initialize(this, GetComponent<TrainMovementController>());
    }

    public void DisplayCart(ECartType cartType, int position)
    {
        _trainCarts[position].SetCartType(cartType);
        _trainCarts[position].gameObject.SetActive(true);
    }

    public void HideCart(int position)
    {
        _trainCarts[position].gameObject.SetActive(false);
    }
}
