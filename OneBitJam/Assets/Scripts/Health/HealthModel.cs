﻿using System.Collections;
using System.Collections.Generic;

public class HealthModel
{
    private Stack<ECartType> _carts;

    public void AddCaboose(ECartType cartType)
    {
        _carts.Push(cartType);
    }

    public ECartType RemoveCart()
    {
        return _carts.Pop();
    }

    public int Health
    {
        get
        {
            return _carts.Count;
        }
    }

    public HealthModel()
    {
        _carts = new Stack<ECartType>();
        _carts.Push(ECartType.Default);
        _carts.Push(ECartType.Default);
        _carts.Push(ECartType.Default);
        _carts.Push(ECartType.Default);
        _carts.Push(ECartType.Default);
    }
}
