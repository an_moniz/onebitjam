﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : SingletonBehaviour<GameManager>
{
    [SerializeField] public float distanceTravelled;
    [SerializeField] public float distanceToNextStop;
    [SerializeField] private DistanceFormulas distanceFormulas;

    [SerializeField] private TrainMovementController train;
    public TrainMovementController Train => train;

    [SerializeField] private HealthView _healthView;
    public HealthView HealthView => _healthView;

    [SerializeField] private Text DistanceTravelledLabel;
    [SerializeField] private Text DistanceUntilNextStop;

    [SerializeField] private BanditSpawner _banditSpawner;

    [SerializeField] private StationController _station = null;
    public StationController Station => _station;
    [SerializeField] private UISpinnerView _spinnerView = null;
    public UISpinnerView SpinnerView => _spinnerView;
    [SerializeField] private RectTransform _gameOverScreen;
    [SerializeField] private RectTransform _startScreen;

    [SerializeField] private int _initialTrainHealth = 5;

    public int RunIndex;

    void Start()
    {
        _startScreen.gameObject.SetActive(true);
        Train.Paused = true;
    }

    private void StartGame()
    {
        RunIndex = 0;
        distanceToNextStop = distanceFormulas.GetDistanceForRun(RunIndex);
        distanceTravelled = 0;
        ResetTrainHealth();
        _banditSpawner.StartSpawnCoroutine();
        _station.StationArrivedEvent += OnStationArrived;
        _spinnerView.SpinEnd += OnSpinEnd;
        _gameOverScreen.gameObject.SetActive(false);
        _startScreen.gameObject.SetActive(false);
        Train.StartEngine();
    }

    private void ResetTrainHealth()
    {
        int health = HealthView.HealthController.Model.Health;
        for (int i = 0; i < health; i++)
        {
            HealthView.HealthController.DetachCart();
        }
        for (int i = 0; i < _initialTrainHealth; i++)
        {
            HealthView.HealthController.AddCart(ECartType.Default);
        }
    }

    public void DamageConductor()
    {
        GameOver();
    }

    private void GameOver()
    {
        _gameOverScreen.gameObject.SetActive(true);
        Train.speed = 0;
        StopGame();
    }

    private void StopGame()
    {
        _station.StationArrivedEvent -= OnStationArrived;
        _spinnerView.SpinEnd -= OnSpinEnd;
        _banditSpawner.StopSpawning();
        Train.StopEngine();
        _banditSpawner.ClearAllBandits();
        foreach(var traincar in HealthView.TrainCartViews)
        {
            traincar.BanditSpawner.ClearAllBandits();
        }
    }

    private void Update()
    {
        if ((_gameOverScreen.gameObject.activeSelf || _startScreen.gameObject.activeSelf) && (Input.GetButton("Brake") ||
            Input.touchCount > 0))
        {
            StartGame();
        }
    }

    void LateUpdate()
    {
        distanceTravelled += train.Speed / 1000f;
        DistanceTravelledLabel.text = $"Distance Travelled: {distanceTravelled.ToString("0.00")} Km";

        if (distanceTravelled >= distanceToNextStop)
        {
            distanceToNextStop += distanceFormulas.GetDistanceForRun(++RunIndex);
        }

        DistanceUntilNextStop.text = $"Distance until next stop: {(distanceToNextStop - distanceTravelled).ToString("0.00")} Km";
    }

    public void OnStationApproaching()
    {
        _banditSpawner.StopSpawning();
    }

    private void OnStationArrived()
    {
        if (HealthView.HealthController.Model.Health < HealthView.TrainCartViews.Count )
        {
            _spinnerView.gameObject.SetActive(true);
            int randomPrize = Random.Range(0, (int)ECartType.Qty);
            _spinnerView.SpinEnd += () => GiveNewCart((ECartType)randomPrize);
            _spinnerView.StartSpin(randomPrize);
        }
        else
        {
            StartCoroutine(StationPause());
        }
    }

    private IEnumerator StationPause()
    {
        yield return new WaitForSeconds(2.6f);
        _spinnerView.SkipSpinner();
    }

    private void GiveNewCart(ECartType cartType)
    {
        _spinnerView.gameObject.SetActive(false);
        HealthView.HealthController.AddCart(cartType);
    }

    private void OnSpinEnd()
    {
        _banditSpawner.StartSpawnCoroutine();
    }
}
