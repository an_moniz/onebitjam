﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ECartType
{
    Default,
    Heavy,
    Slip,
    Bandit,
    Qty
}
