﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationController : MonoBehaviour
{
    bool stationApproaching = true;
    float upcomingStationDistance = 10;

    public event Action StationArrivedEvent;

    // Update is called once per frame
    void Update()
    {
        upcomingStationDistance = GameManager.Instance.distanceToNextStop - GameManager.Instance.distanceTravelled;

        //stop at station
        if ((gameObject.transform.position.x <= 0) && !(GameManager.Instance.Train.stationed) && (stationApproaching))
        {
            GameManager.Instance.Train.stationed = true;
            stationApproaching = false;
            StationArrivedEvent?.Invoke();
        }

        //station movement code
        if (!GameManager.Instance.Train.stationed)
        {
            gameObject.transform.position -= Vector3.right * GameManager.Instance.Train.speed * 0.002f;
        }

        //reset the station offscreen when the upcoming station is close
        if ((upcomingStationDistance <= 10) && !(stationApproaching) && (gameObject.transform.position.x <= -20))
        {
            stationApproaching = true;
            GameManager.Instance.OnStationApproaching();
            gameObject.transform.position = new Vector3(40, gameObject.transform.position.y, gameObject.transform.position.z);
        }
    }
}
