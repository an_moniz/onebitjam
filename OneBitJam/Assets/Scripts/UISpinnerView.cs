﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISpinnerView : MonoBehaviour
{
    [SerializeField] private Image _spinner;
    [SerializeField] private Image _needle;

    [SerializeField] private float[] _rotNotches;

    private float _targetRot = 0.0f;
    [SerializeField] private float _anticipationSpeed;
    [SerializeField] private float _anticipationTime;
    [SerializeField] private float _easeInTime;

    public event Action SpinEnd; 

    public void StartSpin( int prizeIndex )
    {
        int nextIndex = (prizeIndex + 1) % _rotNotches.Length;
        float maxRot = _rotNotches[prizeIndex];
        float minRot = _rotNotches[nextIndex];
        if (minRot > maxRot)
        {
            minRot -= 360f;
        }
        _targetRot = UnityEngine.Random.Range(minRot, maxRot);
        StartCoroutine(RotateToTargetCoroutine());
    }

    private IEnumerator RotateToTargetCoroutine()
    {
        float timePassed = 0.0f;
        float rotZ = 0.0f;
        while (timePassed < _anticipationTime)
        {
            timePassed += Time.deltaTime;
            rotZ -= _anticipationSpeed * Time.deltaTime;
            _needle.transform.rotation = Quaternion.Euler(0.0f, 0.0f, rotZ);
            yield return null;
        }
        timePassed = 0.0f;
        float t = 0.0f;
        float startRot = rotZ % 360;
        while (timePassed < _easeInTime)
        {
            timePassed += Time.deltaTime;
            t = timePassed / _easeInTime;
            _needle.transform.rotation = Quaternion.Euler(0.0f, 0.0f, Easing.EaseOutQuad(startRot - 360f, _targetRot - 360f, t) + 360f);
            yield return null;
        }
        SpinEnd?.Invoke();
        SpinEnd = null;
    }

    private static Quaternion Slerp(Quaternion p, Quaternion q, float t, bool shortWay)
    {
        float dot = Quaternion.Dot(p, q);
        if (shortWay)
        {
            if (dot < 0.0f)
                return Slerp(ScalarMultiply(p, -1.0f), q, t, true);
        }

        float angle = Mathf.Acos(dot);
        Quaternion first = ScalarMultiply(p, Mathf.Sin((1f - t) * angle));
        Quaternion second = ScalarMultiply(q, Mathf.Sin((t) * angle));
        float division = 1f / Mathf.Sin(angle);
        return ScalarMultiply(Add(first, second), division);
    }
    private static Quaternion ScalarMultiply(Quaternion input, float scalar)
    {
        return new Quaternion(input.x * scalar, input.y * scalar, input.z * scalar, input.w * scalar);
    }

    private static Quaternion Add(Quaternion p, Quaternion q)
    {
        return new Quaternion(p.x + q.x, p.y + q.y, p.z + q.z, p.w + q.w);
    }

    public void SkipSpinner()
    {
        SpinEnd?.Invoke();
    }
}
