﻿using UnityEngine;

[CreateAssetMenu(fileName = "SpawnParams", menuName = "ScriptableObjects/SpawnData", order = 1)]
public class SpawnData : ScriptableObject
{
    [System.Serializable]
    public struct SpawnTimeRange
    {
        public float Min;
        public float Max;
    }

    public SpawnTimeRange MinSpawnTimeRange;
    public SpawnTimeRange MaxSpawnTimeRange;

    public float MaxRunIndex = 10f;

    public float GetRandomSpawnTime(int runIndex)
    {
        float t = (float)runIndex / MaxRunIndex;
        if (t > 1.0f) t = 1.0f;
        return Random.Range(Mathf.Lerp(MinSpawnTimeRange.Min, MaxSpawnTimeRange.Min, t), Mathf.Lerp(MinSpawnTimeRange.Max, MaxSpawnTimeRange.Max, t));
    }
}