﻿using UnityEngine;

[CreateAssetMenu(fileName = "BanditParams", menuName = "ScriptableObjects/BanditData", order = 1)]
public class BanditParams : ScriptableObject
{
    public float MinWalkSpeed;
    public float MaxWalkSpeed;

    public float MinTimeToGetScaredChance;
    public float MaxTimeToGetScaredChance;
    public float MinChanceToGetScared;
    public float MaxChanceToGetScared;

    public float TimeToJumpToNextCart;
    public float JumpHeight;

    public float MinHp;
    public float MaxHp;

    public float VulnerabilityMinDmgPerChangeOfSpeed = 0.05f;
    public float VulnerabilityMaxDmgPerChangeOfSpeed = 0.01f;

    public float SlipperyDamageMultiplier = 3f;

    public float MinDmgPerChangeOfSpeed = 0.1f;
    public float MaxDmgPerChangeOfSpeed = 0.1f;

    public float MinComfortToWalkFast;
    public float MaxComfortToWalkFast;

    public float MinComfortToWalkSlowly;
    public float MaxComfortToWalkSlowly;

}