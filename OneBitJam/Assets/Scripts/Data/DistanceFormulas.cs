﻿using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "DistanceFormulas", menuName = "ScriptableObjects/Formulas", order = 1)]
public class DistanceFormulas : ScriptableObject
{
    public float InitialDistance = 400f;
    public float DistancePerRun = 100f;

    public float GetDistanceForRun(int runIndex)
    {
        return InitialDistance + DistancePerRun * runIndex;
    }
}