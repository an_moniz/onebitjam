﻿public interface IState
{
    void Enter();
    void Update();
    void Exit();
}

public class StateMachine
{
    protected IState _currentState;

    public void ChangeState(IState newState)
    {
        if (_currentState != null)
            _currentState.Exit();

        _currentState = newState;
        _currentState.Enter();
    }

    public void Update()
    {
        if (_currentState != null) _currentState.Update();
    }
}
