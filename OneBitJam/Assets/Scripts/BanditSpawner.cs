﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct BanditPrefabSpawnWeight
{
    public BanditController Prefab;
    public float ChanceToSpawn;
}

public class BanditSpawner : MonoBehaviour
{
    [SerializeField] private SpawnData _spawnParams;

    [SerializeField] private BanditPrefabSpawnWeight[] _banditPrefabs;

    [SerializeField] public Transform SpawnPoint;

    private Coroutine _spawningCoroutine;

    private Queue<BanditController> _banditPool;
    private List<BanditController> _spawnedBandits;

    [SerializeField] bool _isTrainSpawner = false;
    private bool _enabled;

    private void Awake()
    {
        _banditPool = new Queue<BanditController>();
        _spawnedBandits = new List<BanditController>();
        _enabled = false;
    }

    private void Pool( BanditController bandit )
    {
        bandit.Reset();
        bandit.gameObject.SetActive(false);
        _spawnedBandits.Remove(bandit);
        _banditPool.Enqueue(bandit);
    }

    private BanditController GetBanditPrefab()
    {
        float roll = Random.value;
        float accumulatedChance = 0.0f;
        int banditPrefabIndex = 0;
        for (int size = _banditPrefabs.Length; banditPrefabIndex < size; banditPrefabIndex++)
        {
            accumulatedChance += _banditPrefabs[banditPrefabIndex].ChanceToSpawn;
            if (roll <= accumulatedChance)
            {
                break;
            }
        }
        return _banditPrefabs[banditPrefabIndex].Prefab;
    }

    private BanditController GetNewBandit()
    {
        if (_banditPool.Count > 0)
        {
            BanditController newBandit = _banditPool.Dequeue();
            newBandit.gameObject.SetActive(true);
            _spawnedBandits.Add(newBandit);
            return newBandit;
        }
        else
        {
            BanditController newBandit = GameObject.Instantiate(GetBanditPrefab());
            _spawnedBandits.Add(newBandit);
            return newBandit;
        }
    }

    private void SpawnBandit()
    {
        BanditController newBandit = GetNewBandit();
        newBandit.transform.position = SpawnPoint.position;
        newBandit.KOEvent += Pool;
        newBandit.Init();
    }

    private IEnumerator SpawnCountdownCoroutine()
    {
        float timeUntilNextSpawn = _spawnParams.GetRandomSpawnTime(GameManager.Instance.RunIndex);
        yield return new WaitForSeconds(timeUntilNextSpawn);
        if (!_enabled) yield break;
        if (!_isTrainSpawner)
            SpawnPoint = GameManager.Instance.HealthView.TrainCartViews[GameManager.Instance.HealthView.HealthController.Model.Health - 1].StartPos;
        else
            Debug.Log(SpawnPoint.position.x);
        SpawnBandit();
        _spawningCoroutine = StartCoroutine("SpawnCountdownCoroutine");
    }

    public void StartSpawnCoroutine()
    {
        if (gameObject.activeSelf)
        {
            _enabled = true;
            _spawningCoroutine = StartCoroutine(SpawnCountdownCoroutine());
        }
    }

    public void StopSpawning()
    {
        _enabled = false;
        StopCoroutine(_spawningCoroutine);
    }

    public void ClearAllBandits()
    {
        if (_spawnedBandits == null) return;
        int numBandits = _spawnedBandits.Count;
        for (int i = numBandits - 1; i >= 0; i--)
        {
            Pool(_spawnedBandits[i]);
        }
    }
}