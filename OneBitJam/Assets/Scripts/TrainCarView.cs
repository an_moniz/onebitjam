﻿using UnityEngine;
using System.Collections.Generic;

public class TrainCarView : MonoBehaviour
{
    [SerializeField] private Transform startPos;
    public Transform StartPos => startPos;
    [SerializeField] private Transform endPos;
    public Transform EndPos => endPos;

    [SerializeField] private List<GameObject> _cartTypes;
    private int _currentCart = 0;
    public int CurrentCart => _currentCart;

    [SerializeField] private BanditSpawner _banditSpawner;
    public BanditSpawner BanditSpawner => _banditSpawner;

    public void SetCartType(ECartType cartType)
    {
        _cartTypes[_currentCart].SetActive(false);
        _currentCart = (int)cartType;
        _cartTypes[_currentCart].SetActive(true);
        if (_currentCart == 3)
            _banditSpawner.StartSpawnCoroutine();
    }
}
