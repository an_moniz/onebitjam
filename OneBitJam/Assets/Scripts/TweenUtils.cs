﻿using UnityEngine;

public static class TweenUtils
{
    public static Vector3 UpdateMove(Vector3 posFrom, Vector3 posTo, float t, Easing.Ease easing)
    {
        return Vector3.Lerp(posFrom, posTo, Easing.EaseResult(easing, 0f, 1f, t));
    }

    public static Vector3 UpdateMoveArc(Vector3 posFrom, Vector3 posTo, float arcHeight, float t, Easing.Ease easing)
    {
        Vector3 pos = Vector3.Lerp(posFrom, posTo, Easing.EaseResult(easing, 0f, 1f, t));
        pos.y += arcHeight * Mathf.Sin(Mathf.Clamp01(Easing.EaseResult(easing, 0f, 1f, t)) * Mathf.PI);
        return pos;
    }
}

public static class Easing
{
    public enum Ease
    {
        Linear = 0,
        EaseInQuad,
        EaseOutQuad,
        EaseInOutQuad
    }

    public static float Linear(float start, float end, float t)
    {
        return Mathf.Lerp(start, end, t);
    }

    public static float EaseInQuad(float start, float end, float t)
    {
        return (end - start) * t * t + start;
    }

    public static float EaseOutQuad(float start, float end, float t)
    {
        return -(end - start) * t * (t - 2) + start;
    }

    public static float EaseInOutQuad(float start, float end, float t)
    {
        t /= .5f;
        end -= start;
        if (t < 1) return end * 0.5f * t * t + start;
        t--;
        return -end * 0.5f * (t * (t - 2) - 1) + start;
    }

    public delegate float Function(float s, float e, float v);

    public static Function GetEasingFunction(Ease easingFunction)
    {
        if (easingFunction == Ease.Linear)
        {
            return Linear;
        }
        else if (easingFunction == Ease.EaseInQuad)
        {
            return EaseInQuad;
        }
        else if (easingFunction == Ease.EaseOutQuad)
        {
            return EaseOutQuad;
        }
        else if (easingFunction == Ease.EaseInOutQuad)
        {
            return EaseInOutQuad;
        }
        return Linear;
    }

    public static float EaseResult(Ease easingFunction, float start, float end, float t)
    {
        Function func = GetEasingFunction(easingFunction);
        return func(start, end, t);
    }
}