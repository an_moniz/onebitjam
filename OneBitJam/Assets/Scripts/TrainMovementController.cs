﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrainMovementController : MonoBehaviour
{
    [SerializeField] Text speedometer;
    //[SerializeField] GameObject Background1;
    //[SerializeField] GameObject Background2;
    [SerializeField] List<GameObject> Track;
    [SerializeField] List<GameObject> Trees;
    [SerializeField] List<GameObject> Mountains;
    [SerializeField] List<GameObject> Clouds;
    [SerializeField] List<Animator> Wheels;

    //Train Movement
    public float speed = 0;
    public float maxSpeed = 60;
    public float acceleration = 4;
    public float drag = 0f;
    public float brakePower = 20;
    public bool stationed = false;
    public float wheel_speed = 0f;

    //paralax background
    public float trackLayer_scrollSpeed = 0.002f;
    public float mountainLayer_scrollSpeed = 0.001f;
    public float cloudLayer_scrollSpeed = 0.0005f;

    //Audio
    private AudioSource _as;
    public AudioClip[] chugachuga;
    float chugPhase = 0;

    private float _lastSpeed;
    public float Speed { get { return speed; } }
    public float ChangeOfSpeed { get; private set; }
    public float Acceleration { get { return acceleration * (1 - drag); } }
    public bool Paused = false;

    public event Action BrakeEvent;

    void Accelerate() {
        if (!stationed)
        {
            if (speed < maxSpeed) {
                speed += (acceleration + (speed * 0.1f)) * Time.deltaTime;
            }
            if (speed > maxSpeed) speed = maxSpeed;
        } else
        {
            speed = 0;
        }
    }

    void Brake() {
        if (speed > 0) {
            speed -= brakePower * Time.deltaTime;
            BrakeEvent?.Invoke();
        }
        if (speed < 0) speed = 0;
    }
    
    public void ModifyDrag(float dragConst)
    {
        drag += dragConst;
    }

    void AnimateBackground()
    {
        //TRAIN TRACK
        for (int i = 0; i < Track.Count; i++)
        {
            Track[i].transform.position -= Vector3.right * speed * trackLayer_scrollSpeed;
            if (Track[i].transform.position.x <= -10)
            {
                Track[i].transform.position = new Vector3(10f, Track[i].transform.position.y, Track[i].transform.position.z);
            }
        }

        //TREE LINE
        for (int i = 0; i < Trees.Count; i++)
        {
            Trees[i].transform.position -= Vector3.right * speed * trackLayer_scrollSpeed;
            if (Trees[i].transform.position.x <= -10)
            {
                Trees[i].transform.position = new Vector3(10f, Trees[i].transform.position.y, Trees[i].transform.position.z);
            }
        }

        //MOUNTAIN RANGE
        for (int i = 0; i < Mountains.Count; i++)
        {
            Mountains[i].transform.position -= Vector3.right * speed * mountainLayer_scrollSpeed;
            if (Mountains[i].transform.position.x <= -10)
            {
                Mountains[i].transform.position = new Vector3(27f, Mountains[i].transform.position.y, Mountains[i].transform.position.z);
            }
        }

        //CLOUDS
        for (int i = 0; i < Clouds.Count; i++)
        {
            Clouds[i].transform.position -= Vector3.right * speed * cloudLayer_scrollSpeed;
            if (Clouds[i].transform.position.x <= -10)
            {
                Clouds[i].transform.position = new Vector3(10f, Clouds[i].transform.position.y, Clouds[i].transform.position.z);
            }
        }
    }
    
    void ChugaChuga()
    {
        //play chugachuga sfx according to the speed of the train
        float chugRate = (160 - (speed * 2));

        if (speed > 0)
        {
            if (chugPhase >= chugRate)
            {
                _as.PlayOneShot(_as.clip);
                chugPhase = 0;
            } else
            {
                chugPhase++;
                _as.clip = chugachuga[UnityEngine.Random.Range(0, chugachuga.Length)];
            }
        }
        
    }

    public void StartEngine()
    {
        //chugachuga = Resources.Load<AudioClip>("chugachuga");

        _as = GetComponent<AudioSource>();
        _as.clip = chugachuga[UnityEngine.Random.Range(0, chugachuga.Length)];
        _as.PlayOneShot(_as.clip);

        GameManager.Instance.SpinnerView.SpinEnd += LeaveStation;
        Paused = false;
    }

    public void StopEngine()
    {
        GameManager.Instance.SpinnerView.SpinEnd -= LeaveStation;
        Paused = true;
        speed = 0;
    }

    private void OnDestroy()
    {
        StopEngine();
    }

    void Update()
    {

        if (Paused) return;
        if (Input.GetButton("Brake") ||
            Input.touchCount > 0) {
            if (stationed)
            {
                return;
            } else
            {
                Brake();
            }
        } else {
            Accelerate();
        }
        ChangeOfSpeed = speed - _lastSpeed;
        _lastSpeed = speed;
        speedometer.text = "Speed: " + Mathf.Round(speed);
        AnimateBackground();
        ChugaChuga();

        foreach(Animator wheel_animator in Wheels)
        {
            wheel_speed = 0.1f * speed;
            wheel_animator.SetFloat("speed", wheel_speed);
        }
    }

    public void LeaveStation()
    {
        stationed = false;
    }

}
