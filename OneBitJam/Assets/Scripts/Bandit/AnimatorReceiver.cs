﻿using System;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AnimatorReceiver : MonoBehaviour
{
    public event Action<string> AnimEnd;
    public void OnAnimEnd(string str)
    {
        AnimEnd?.Invoke(str);
    }
}