﻿using UnityEngine;
class BanditWalkRoutineState : BanditState
{
    private Vector3 _destination;
    private float _comfort;

    private float _timeSinceCourageDropped;
    private float _timeUntilScareChance = 0.0f;
    private float _scareChance = 0.0f;
    public override BanditStateType GetStateType() { return BanditStateType.WalkRoutine; }

    public BanditWalkRoutineState( BanditController bandit )
    {
        _bandit = bandit;
    }

    public override void Enter()
    {
        // Get Destination
        _destination = _bandit.GetCartEndPos(_bandit.GetCartIndex()).position;
        _comfort = 0.0f;
        _timeSinceCourageDropped = 0.0f;
        SetupScareChance();
    }

    private void SetupScareChance()
    {
        _timeUntilScareChance = Random.Range(_bandit.MinTimeToGetScaredChance, _bandit.MaxTimeToGetScaredChance);
        _scareChance = Random.Range(_bandit.MinChanceToGetScared, _bandit.MaxChanceToGetScared);
    }

    private float CalculateWalkSpeed( float courage )
    {
        float walkSpeed = 0.0f;
        if (_comfort >= 0.6f)
        {
            float diffToCap = _comfort - 0.6f;
            float ratioToMax = diffToCap / (1.0f - 0.6f);
            walkSpeed = (_bandit.MaxWalkSpeed - _bandit.MinWalkSpeed) * ratioToMax + _bandit.MinWalkSpeed;
        }
        else if (_comfort >= 0.3f)
        {
            walkSpeed = _bandit.MinWalkSpeed;
        }
        return walkSpeed;
    }

    public override void Exit()
    {
    }

    public override void Update()
    {
        bool destinationReached = false;

        if (_timeSinceCourageDropped < _bandit.TimeToReachMaxCourage)
        {
            _timeSinceCourageDropped += Time.deltaTime;
            float t = _timeSinceCourageDropped / _bandit.TimeToReachMaxCourage;
            _comfort = Easing.EaseResult(Easing.Ease.Linear, 0.0f, 1.0f, t);
        }

        if (_timeUntilScareChance > 0.0f)
        {
            _timeUntilScareChance -= Time.deltaTime;
            if (_timeUntilScareChance <= 0.0f)
            {
                float roll = Random.value;
                if (roll <= _scareChance)
                {
                    _comfort = 0.0f;
                    _timeSinceCourageDropped = 0.0f;
                }
                SetupScareChance();
            }
        }

        float walkSpeed = CalculateWalkSpeed(_comfort);

        Vector3 difference = (_destination - _bandit.transform.position);
        difference.z = 0f;
        Vector3 walkVector = walkSpeed * difference.normalized * Time.deltaTime;
        if (walkVector.x + _bandit.transform.position.x >= _destination.x)
        {
            walkVector.x = (_destination.x - _bandit.transform.position.x);
            destinationReached = true;
        }
        _bandit.transform.position += walkVector;

        _bandit.Vulnerability = 1.0f - _comfort;
        _bandit.Animator.SetFloat("Speed", walkSpeed / _bandit.MaxWalkSpeed);

        if (destinationReached)
        {
            UseAbility();
        }
    }

    public virtual void UseAbility()
    {
        if (_bandit.GetNextCartIndex() >= 0)
        {
            _bandit.ChangeState(BanditStateType.JumpToNextCart);
        }
        else if (_bandit.GetCartIndex() == 0)
        {
            _bandit.ChangeState(BanditStateType.Attack);
        }
    }

    public override void NotifyBreak()
    {
          _bandit.ChangeState(BanditStateType.Cling);
    }
}