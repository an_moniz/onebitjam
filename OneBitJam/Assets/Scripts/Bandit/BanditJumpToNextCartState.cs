﻿using UnityEngine;

public class BanditJumpToNextCartState : BanditState
{
    private Vector3 _destination;
    private Vector3 _startPosition;
    private float _timePassed;

    public override BanditStateType GetStateType() { return BanditStateType.JumpToNextCart; }
    public BanditJumpToNextCartState( BanditController banditController )
    {
        _bandit = banditController;
    }

    public override void Enter()
    {
        // Set Jump Destination
        // Start Jump
        int currentCartIndex = _bandit.GetCartIndex();
        int nextCartIndex = _bandit.GetNextCartIndex();
        _startPosition = _bandit.transform.position;
        _destination = _bandit.GetCartStartPos(nextCartIndex).position;
        _timePassed = 0.0f;
    }

    public override void Exit()
    {
    }

    public override void Update()
    {
        // Progress Jump
        _timePassed += Time.deltaTime;
        float t = _timePassed / _bandit.TimeToJumpToNextCart;
        if (t > 1) t = 1;
        _bandit.transform.position = TweenUtils.UpdateMoveArc(_startPosition, _destination, _bandit.JumpHeight, t, Easing.Ease.EaseInQuad);
        if ( t >= 1 )
        {
            _bandit.ChangeState( BanditStateType.WalkRoutine);
        }
    }

    public override void NotifyBreak()
    {
        // KO
    }
}