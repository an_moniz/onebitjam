﻿using UnityEngine;

public class BanditAttackState : BanditState
{
    public override BanditStateType GetStateType()
    {
        return BanditStateType.Attack;
    }

    public BanditAttackState( BanditController banditState )
    {
        _bandit = banditState;
    }

    public override void Enter()
    {
        _bandit.Animator.SetTrigger("Attack");
    }

    public override void Update()
    {
        var state = _bandit.Animator.GetCurrentAnimatorStateInfo(0);
        if (state.IsName("Attack") && state.normalizedTime > 1.0f)
        {
            GameManager.Instance.DamageConductor();
            _bandit.KO();
        }
    }

    public override void Exit()
    {
        _bandit.Animator.ResetTrigger("Attack");
    }

    public override void NotifyBreak()
    {
        _bandit.ChangeState(BanditStateType.KO);
    }

}