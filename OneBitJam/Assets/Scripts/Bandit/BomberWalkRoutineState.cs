﻿
class BomberWalkRoutineState : BanditWalkRoutineState
{
    public BomberWalkRoutineState(BanditController bandit) : base(bandit)
    {
        _bandit = bandit;
    }

    public override void UseAbility()
    {
        _bandit.ChangeState(BanditStateType.Explode);
    }
}
