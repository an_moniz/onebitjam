﻿public enum BanditStateType
{
    WalkRoutine,
    JumpToNextCart,
    Cling,
    KO,
    Explode,
    Attack,
    State_Qty,
}