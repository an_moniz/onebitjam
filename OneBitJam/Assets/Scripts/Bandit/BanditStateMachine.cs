﻿public class BanditStateMachine : StateMachine
{
    public BanditStateType GetCurrentState()
    {
        return ((BanditState)_currentState).GetStateType();
    }
    public void NotifyBreak()
    {
        ((BanditState)_currentState).NotifyBreak();
    }
}