﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BomberExplodeRoutineState : BanditState
{
    public BomberExplodeRoutineState(BanditController bandit)
    {
        _bandit = bandit;
    }

    public override void Enter()
    {
        if (_bandit.GetNextCartIndex() >= 0)
        {
            _bandit.Animator.SetBool("Explode", true);
        }
        else
        {
            //GameOver();
        }
    }

    public override void Exit()
    {

    }

    public override BanditStateType GetStateType()
    {
        return BanditStateType.Explode;
    }

    public override void NotifyBreak()
    {
        
    }

    public override void Update()
    {
        
    }
}
