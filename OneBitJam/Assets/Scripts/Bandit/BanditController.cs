﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BanditController : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    public Animator Animator => _animator;
    [SerializeField] AnimatorReceiver _animatorReceiver;
    [SerializeField] private SpriteRenderer _spriteRenderer;

    private BanditStateMachine _stateMachine = new BanditStateMachine();
    private BanditState[] _banditStates;

    [SerializeField] private float _minWalkSpeed;
    [SerializeField] private float _maxWalkSpeed;

    [SerializeField] private float _minTimeToGetScaredChance;
    [SerializeField] private float _maxTimeToGetScaredChance;
    [SerializeField] private float _minChanceToGetScared;
    [SerializeField] private float _maxChanceToGetScared;
    public float MinTimeToGetScaredChance => _minTimeToGetScaredChance;
    public float MaxTimeToGetScaredChance => _maxTimeToGetScaredChance;
    public float MinChanceToGetScared => _minChanceToGetScared;
    public float MaxChanceToGetScared => _maxChanceToGetScared;

    [SerializeField] private float _timeToJumpToNextCart;
    public float TimeToJumpToNextCart => _timeToJumpToNextCart;
    [SerializeField] private float _jumpHeight;

    public float TimeToReachMaxCourage;
    public float JumpHeight => _jumpHeight;

    [SerializeField] private float _MaxHp = 5.0f;

    public float Vulnerability { get; set; } = 0.0f;
    public float HP;

    public float MinWalkSpeed => _minWalkSpeed;
    public float MaxWalkSpeed => _maxWalkSpeed;

    [SerializeField] private Transform _fallPosition;
    public Transform FallPosition => _fallPosition;
    [SerializeField] private float _fallPeak = 20f;
    public float FallPeak => _fallPeak;

    public event Action<BanditController> KOEvent;

    [SerializeField] private EBanditType _banditType;

    [SerializeField] private BanditParams _banditParams;
    public BanditParams BanditParams => _banditParams;

    //Audio
    public AudioSource _as;
    public AudioClip[] scream;

    public void Reset()
    {
        KOEvent = null;
        _banditStates = new BanditState[((int)BanditStateType.State_Qty)];
        GameManager.Instance.Train.BrakeEvent -= NotifyBrake;
        GameManager.Instance.HealthView.HealthController.CartDetachEvent -= NotifyCartDied;
        GameManager.Instance.Station.StationArrivedEvent -= InstantKO;
        Animator.SetBool("Cling", false);
        Animator.SetBool("KO", false);
        Animator.SetBool("Explode", false);
        Animator.ResetTrigger("Attack");
        Animator.ResetTrigger("Reset");
    }

    public void Init()
    {
        Vulnerability = 0.0f;
        HP = UnityEngine.Random.Range(_banditParams.MinHp, _banditParams.MaxHp);
        Animator.SetTrigger("Reset");
        ChangeState(BanditStateType.WalkRoutine);
        GameManager.Instance.Train.BrakeEvent += NotifyBrake;
        GameManager.Instance.HealthView.HealthController.CartDetachEvent += NotifyCartDied;
        GameManager.Instance.Station.StationArrivedEvent += InstantKO;
    }

    private void Awake()
    {
        _banditStates = new BanditState[((int)BanditStateType.State_Qty)];
        _animatorReceiver.AnimEnd += OnAnimEnd;
    }

    private void Start()
    {
        ChangeState( BanditStateType.WalkRoutine );
        GameManager.Instance.Train.BrakeEvent += NotifyBrake;

        //audio
        _as = GetComponent<AudioSource>();
        _as.clip = scream[UnityEngine.Random.Range(0, scream.Length)];
        
    }

    private void Update()
    {
        _stateMachine.Update();
    }

    public void InstantKO()
    {
        ChangeState(BanditStateType.KO);
    }

    public void ChangeState( BanditStateType state )
    {
        int stateIndex = (int)state;
        if (_banditStates.Length < stateIndex)
        {
            _stateMachine.ChangeState(_banditStates[stateIndex]);
        }
        else
        {
            BanditState banditState = null;
            switch (state)
            {
                case BanditStateType.WalkRoutine:
                    banditState = _banditType == EBanditType.Bomber ?
                        new BomberWalkRoutineState(this) :
                        new BanditWalkRoutineState(this);
                    break;
                case BanditStateType.JumpToNextCart:
                    banditState = new BanditJumpToNextCartState(this);
                    break;
                case BanditStateType.Cling:
                    banditState = new BanditClingState(this);
                    break;
                case BanditStateType.KO:
                    banditState = new BanditKOState(this);
                    break;
                case BanditStateType.Explode:
                    banditState = new BomberExplodeRoutineState(this);
                    break;
                case BanditStateType.Attack:
                    banditState = new BanditAttackState(this);
                    break;
                default:
                    break;
            }
            if (banditState != null)
            {
                _banditStates[stateIndex] = banditState;
                _stateMachine.ChangeState(banditState);
            }
        }
    }

    public int GetCartIndex()
    {
        int cartIndex = -1;
        float banditX = transform.position.x;
        var trainCartViews = GameManager.Instance.HealthView.TrainCartViews;
        int lastCartIndex = trainCartViews.Count - 1;
        for(int i = lastCartIndex; i >= 0; i--)
        {
            var trainCartView = trainCartViews[i];
            if (banditX >= trainCartView.StartPos.position.x && 
                banditX <= trainCartView.EndPos.position.x)
            {
                cartIndex = i;
            }
        }
        return cartIndex;
    }

    public Transform GetCartStartPos(int cartIndex)
    {
        var trainCartViews = GameManager.Instance.HealthView.TrainCartViews;
        return trainCartViews[cartIndex].StartPos;
    }

    public Transform GetCartEndPos( int cartIndex )
    {
        var trainCartViews = GameManager.Instance.HealthView.TrainCartViews;
        return trainCartViews[cartIndex].EndPos;
    }

    public int GetNextCartIndex()
    {
        int currentCartIndex = GetCartIndex();
        return GetCartIndex() - 1;
    }

    public void AttackCart( int cartIndex )
    {

    }

    public void NotifyBrake()
    {
        _stateMachine.NotifyBreak();
    }

    public void NotifyCartDied()
    {
        if (GameManager.Instance.HealthView.HealthController.Model.Health <= GetCartIndex())
        {
            if (_stateMachine.GetCurrentState() != BanditStateType.Explode &&
                _stateMachine.GetCurrentState() != BanditStateType.KO)
            {
                ChangeState(BanditStateType.KO);
            }
        }
    }

    private void OnAnimEnd(string anim)
    {

    }

    public void KO()
    {
        KOEvent?.Invoke(this);
    }
}
