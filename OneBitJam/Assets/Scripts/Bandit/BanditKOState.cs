﻿using UnityEngine;

public class BanditKOState : BanditState
{
    private Vector3 _destination;
    private Vector3 _startPosition;
    private float _timePassed;
    private Vector3 _passageOfTrain;

    public BanditKOState( BanditController bandit )
    {
        _bandit = bandit;
    }

    public override BanditStateType GetStateType()
    {
        return BanditStateType.KO;
    }
    public override void Enter()
    {
        //_bandit._as.PlayOneShot(_bandit._as.clip);

        _bandit.Animator.SetBool("KO", true);
        _startPosition = _bandit.transform.position;
        _destination = _bandit.FallPosition.position + Vector3.right * GameManager.Instance.Train.Speed;
        _timePassed = 0.0f;
        _passageOfTrain = Vector3.zero;
    }

    public override void Update()
    {
        _timePassed += Time.deltaTime;
        float t = _timePassed / 2.0f;
        if (t > 1) t = 1f;
        Vector3 newPos = TweenUtils.UpdateMoveArc(_startPosition, _destination, _bandit.FallPeak, t, Easing.Ease.EaseInQuad);
        _passageOfTrain += Vector3.right * -GameManager.Instance.Train.ChangeOfSpeed * Time.deltaTime;
        _bandit.transform.position = newPos;
        if (t >= 1f)
        {
            _bandit.KO();
        }
    }

    public override void Exit()
    {
    }

    public override void NotifyBreak()
    {
    }
}