﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BanditExploder : MonoBehaviour
{
    BanditController _controller;

    // Start is called before the first frame update
    void Start()
    {
        _controller = GetComponentInParent<BanditController>();
    }

    public void Explode()
    {
        GameManager.Instance.HealthView.HealthController.DetachCart();
        _controller.KO();
    }
}
