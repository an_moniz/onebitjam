﻿public abstract class BanditState : IState
{
    protected BanditController _bandit;

    public abstract BanditStateType GetStateType();
    public abstract void Enter();
    public abstract void Exit();
    public abstract void Update();

    public abstract void NotifyBreak();
}