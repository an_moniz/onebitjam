﻿using UnityEngine;

public class BanditClingState : BanditState
{
    bool _isSlippery = false;

    public BanditClingState( BanditController bandit)
    {
        _bandit = bandit;
    }
    public override BanditStateType GetStateType()
    {
        return BanditStateType.Cling;
    }
    public override void Enter()
    {
        _bandit.Animator.SetFloat("Speed", 0);
        //Check if current cart is slippery
        _isSlippery = GameManager.Instance.HealthView.TrainCartViews[_bandit.GetCartIndex()].CurrentCart == 2;
    }
    
    public override void Exit()
    {
        _bandit.Animator.SetBool("Cling", false);
    }

    public override void Update()
    {
        if (GameManager.Instance.Train.ChangeOfSpeed < 0)
        {
            if (_bandit.Vulnerability > 0)
            {
                float vulnerabilityDamage = Random.Range(_bandit.BanditParams.VulnerabilityMinDmgPerChangeOfSpeed, _bandit.BanditParams.VulnerabilityMaxDmgPerChangeOfSpeed);
                // Apply Damage to Vulnerability
                _bandit.Vulnerability += _isSlippery ?
                    GameManager.Instance.Train.ChangeOfSpeed * vulnerabilityDamage * _bandit.BanditParams.SlipperyDamageMultiplier : 
                    GameManager.Instance.Train.ChangeOfSpeed * vulnerabilityDamage;
            }
            else
            {
                _bandit.Animator.SetBool("Cling", true);

                float damage = Random.Range(_bandit.BanditParams.VulnerabilityMinDmgPerChangeOfSpeed, _bandit.BanditParams.VulnerabilityMaxDmgPerChangeOfSpeed);

                _bandit.HP += _isSlippery ?
                    GameManager.Instance.Train.ChangeOfSpeed * damage * _bandit.BanditParams.SlipperyDamageMultiplier :
                    GameManager.Instance.Train.ChangeOfSpeed * damage; 
                if (_bandit.HP <= 0)
                {
                    _bandit.ChangeState(BanditStateType.KO);
                    _bandit._as.PlayOneShot(_bandit._as.clip);
                }
            }
        }
        else
        {
            _bandit.ChangeState(BanditStateType.WalkRoutine);
        }
    }

    public override void NotifyBreak()
    {
        // KO
    }
}